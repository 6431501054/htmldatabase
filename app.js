const express = require('express');
const app = express();
const path = require("path");
const con = require('./config/db');
const bcrypt = require('bcrypt');
const { raw } = require('mysql2');
app.use("/public", express.static(path.join(__dirname, "public")));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


// === web sevices ===
app.get('/password/:raw', function(req,res){
    const raw = req.params.raw;
    bcrypt.hash(raw,10, function(err,hash){
        if(err){
            res.status(500).send('Hash error');
        }
        else{
            res.send(hash);
        }
    });
});


//login
app.post('/login', function (req, res) {
    const { username, password } = req.body;
    const sql = "SELECT * FROM user WHERE username=?";
    con.query(sql,[username], function(err,results){
        if(err){
            console.error(err);
            res.status(500).send('DB error')
        }
        else if(results.length != 1){
            res.status(401).send('Wrong username')
        }
        else{
          //compare raw with hashed password
          bcrypt.compare(password, results[0].password, function(err, same){
            if(err){
                res.status(500).send('Password error');
            }
            else{
                if(same == true){
                    res.send("/welcome");
                }
                else{
                    res.status(401).send('Wrong password');
                }
            }
          })
        }
    })
});


// ------------- Welcome --------------
 app.get("/welcome", function (req, res) {
   res.sendFile(path.join(__dirname, "views/welcome_template_add.html"));
    });

// ============= Root ==============
app.get("/", function (_req, res) {
    res.sendFile(path.join(__dirname, "views/index.html"));
});

const port = 3000;
app.listen(port, function () {
    console.log("Server is ready at " + port);
});